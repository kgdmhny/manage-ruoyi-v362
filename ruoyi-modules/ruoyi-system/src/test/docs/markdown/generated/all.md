# 系统模块接口文档


<a name="overview"></a>
## Overview

### License information
*License* : Powered By ruoyi123  
*License URL* : https://127.0.0.1:8888  
*Terms of service* : null


### URI scheme
*Host* : localhost:9201  
*BasePath* : /


### Tags

* sys-config-controller : Sys Config Controller
* sys-dept-controller : Sys Dept Controller
* sys-dict-data-controller : Sys Dict Data Controller
* sys-dict-type-controller : Sys Dict Type Controller
* sys-logininfor-controller : Sys Logininfor Controller
* sys-menu-controller : Sys Menu Controller
* sys-notice-controller : Sys Notice Controller
* sys-operlog-controller : Sys Operlog Controller
* sys-post-controller : Sys Post Controller
* sys-profile-controller : Sys Profile Controller
* sys-role-controller : Sys Role Controller
* sys-user-controller : Sys User Controller
* sys-user-online-controller : Sys User Online Controller




<a name="paths"></a>
## Paths

<a name="addusingpost"></a>
### add
```
POST /config
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**config**  <br>*required*|config|[SysConfig](#sysconfig)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-config-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="editusingput"></a>
### edit
```
PUT /config
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**config**  <br>*required*|config|[SysConfig](#sysconfig)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-config-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="getconfigkeyusingget"></a>
### getConfigKey
```
GET /config/configKey/{configKey}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**configKey**  <br>*required*|configKey|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-config-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="exportusingpost"></a>
### export
```
POST /config/export
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**configId**  <br>*optional*|integer (int64)|
|**Query**|**configKey**  <br>*optional*|string|
|**Query**|**configName**  <br>*optional*|string|
|**Query**|**configType**  <br>*optional*|string|
|**Query**|**configValue**  <br>*optional*|string|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|No Content|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-config-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="listusingget"></a>
### list
```
GET /config/list
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**configId**  <br>*optional*|integer (int64)|
|**Query**|**configKey**  <br>*optional*|string|
|**Query**|**configName**  <br>*optional*|string|
|**Query**|**configType**  <br>*optional*|string|
|**Query**|**configValue**  <br>*optional*|string|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TableDataInfo](#tabledatainfo)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-config-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="refreshcacheusingdelete"></a>
### refreshCache
```
DELETE /config/refreshCache
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-config-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="removeusingdelete"></a>
### remove
```
DELETE /config/{configIds}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**configIds**  <br>*required*|configIds|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-config-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="getinfousingget"></a>
### getInfo
```
GET /config/{configId}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**configId**  <br>*required*|configId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-config-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="addusingpost_1"></a>
### add
```
POST /dept
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**dept**  <br>*required*|dept|[SysDept](#sysdept)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-dept-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="editusingput_1"></a>
### edit
```
PUT /dept
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**dept**  <br>*required*|dept|[SysDept](#sysdept)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-dept-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="listusingget_1"></a>
### list
```
GET /dept/list
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**ancestors**  <br>*optional*|string|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**delFlag**  <br>*optional*|string|
|**Query**|**deptId**  <br>*optional*|integer (int64)|
|**Query**|**deptName**  <br>*optional*|string|
|**Query**|**email**  <br>*optional*|string|
|**Query**|**leader**  <br>*optional*|string|
|**Query**|**orderNum**  <br>*optional*|integer (int32)|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**parentId**  <br>*optional*|integer (int64)|
|**Query**|**parentName**  <br>*optional*|string|
|**Query**|**phone**  <br>*optional*|string|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-dept-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="excludechildusingget"></a>
### excludeChild
```
GET /dept/list/exclude/{deptId}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**deptId**  <br>*required*|deptId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-dept-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="getinfousingget_1"></a>
### getInfo
```
GET /dept/{deptId}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**deptId**  <br>*required*|deptId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-dept-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="removeusingdelete_1"></a>
### remove
```
DELETE /dept/{deptId}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**deptId**  <br>*required*|deptId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-dept-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="addusingpost_2"></a>
### add
```
POST /dict/data
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**dict**  <br>*required*|dict|[SysDictData](#sysdictdata)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-dict-data-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="editusingput_2"></a>
### edit
```
PUT /dict/data
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**dict**  <br>*required*|dict|[SysDictData](#sysdictdata)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-dict-data-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="exportusingpost_1"></a>
### export
```
POST /dict/data/export
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**cssClass**  <br>*optional*|string|
|**Query**|**default**  <br>*optional*|boolean|
|**Query**|**dictCode**  <br>*optional*|integer (int64)|
|**Query**|**dictLabel**  <br>*optional*|string|
|**Query**|**dictSort**  <br>*optional*|integer (int64)|
|**Query**|**dictType**  <br>*optional*|string|
|**Query**|**dictValue**  <br>*optional*|string|
|**Query**|**isDefault**  <br>*optional*|string|
|**Query**|**listClass**  <br>*optional*|string|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|No Content|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-dict-data-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="listusingget_2"></a>
### list
```
GET /dict/data/list
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**cssClass**  <br>*optional*|string|
|**Query**|**default**  <br>*optional*|boolean|
|**Query**|**dictCode**  <br>*optional*|integer (int64)|
|**Query**|**dictLabel**  <br>*optional*|string|
|**Query**|**dictSort**  <br>*optional*|integer (int64)|
|**Query**|**dictType**  <br>*optional*|string|
|**Query**|**dictValue**  <br>*optional*|string|
|**Query**|**isDefault**  <br>*optional*|string|
|**Query**|**listClass**  <br>*optional*|string|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TableDataInfo](#tabledatainfo)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-dict-data-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="dicttypeusingget"></a>
### dictType
```
GET /dict/data/type/{dictType}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**dictType**  <br>*required*|dictType|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-dict-data-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="removeusingdelete_2"></a>
### remove
```
DELETE /dict/data/{dictCodes}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**dictCodes**  <br>*required*|dictCodes|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-dict-data-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="getinfousingget_2"></a>
### getInfo
```
GET /dict/data/{dictCode}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**dictCode**  <br>*required*|dictCode|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-dict-data-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="addusingpost_3"></a>
### add
```
POST /dict/type
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**dict**  <br>*required*|dict|[SysDictType](#sysdicttype)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-dict-type-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="editusingput_3"></a>
### edit
```
PUT /dict/type
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**dict**  <br>*required*|dict|[SysDictType](#sysdicttype)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-dict-type-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="exportusingpost_2"></a>
### export
```
POST /dict/type/export
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**dictId**  <br>*optional*|integer (int64)|
|**Query**|**dictName**  <br>*optional*|string|
|**Query**|**dictType**  <br>*optional*|string|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|No Content|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-dict-type-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="listusingget_3"></a>
### list
```
GET /dict/type/list
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**dictId**  <br>*optional*|integer (int64)|
|**Query**|**dictName**  <br>*optional*|string|
|**Query**|**dictType**  <br>*optional*|string|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TableDataInfo](#tabledatainfo)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-dict-type-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="optionselectusingget"></a>
### optionselect
```
GET /dict/type/optionselect
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-dict-type-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="refreshcacheusingdelete_1"></a>
### refreshCache
```
DELETE /dict/type/refreshCache
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-dict-type-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="removeusingdelete_3"></a>
### remove
```
DELETE /dict/type/{dictIds}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**dictIds**  <br>*required*|dictIds|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-dict-type-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="getinfousingget_3"></a>
### getInfo
```
GET /dict/type/{dictId}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**dictId**  <br>*required*|dictId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-dict-type-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="addusingpost_4"></a>
### add
```
POST /logininfor
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**logininfor**  <br>*required*|logininfor|[SysLogininfor](#syslogininfor)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-logininfor-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="cleanusingdelete"></a>
### clean
```
DELETE /logininfor/clean
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-logininfor-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="exportusingpost_3"></a>
### export
```
POST /logininfor/export
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**accessTime**  <br>*optional*|string (date-time)|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**infoId**  <br>*optional*|integer (int64)|
|**Query**|**ipaddr**  <br>*optional*|string|
|**Query**|**msg**  <br>*optional*|string|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|
|**Query**|**userName**  <br>*optional*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|No Content|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-logininfor-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="listusingget_4"></a>
### list
```
GET /logininfor/list
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**accessTime**  <br>*optional*|string (date-time)|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**infoId**  <br>*optional*|integer (int64)|
|**Query**|**ipaddr**  <br>*optional*|string|
|**Query**|**msg**  <br>*optional*|string|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|
|**Query**|**userName**  <br>*optional*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TableDataInfo](#tabledatainfo)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-logininfor-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="unlockusingget"></a>
### unlock
```
GET /logininfor/unlock/{userName}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**userName**  <br>*required*|userName|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-logininfor-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="removeusingdelete_4"></a>
### remove
```
DELETE /logininfor/{infoIds}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**infoIds**  <br>*required*|infoIds|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-logininfor-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="addusingpost_5"></a>
### add
```
POST /menu
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**menu**  <br>*required*|menu|[SysMenu](#sysmenu)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-menu-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="editusingput_4"></a>
### edit
```
PUT /menu
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**menu**  <br>*required*|menu|[SysMenu](#sysmenu)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-menu-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="getroutersusingget"></a>
### getRouters
```
GET /menu/getRouters
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-menu-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="listusingget_5"></a>
### list
```
GET /menu/list
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**component**  <br>*optional*|string|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**icon**  <br>*optional*|string|
|**Query**|**isCache**  <br>*optional*|string|
|**Query**|**isFrame**  <br>*optional*|string|
|**Query**|**menuId**  <br>*optional*|integer (int64)|
|**Query**|**menuName**  <br>*optional*|string|
|**Query**|**menuType**  <br>*optional*|string|
|**Query**|**orderNum**  <br>*optional*|integer (int32)|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**parentId**  <br>*optional*|integer (int64)|
|**Query**|**parentName**  <br>*optional*|string|
|**Query**|**path**  <br>*optional*|string|
|**Query**|**perms**  <br>*optional*|string|
|**Query**|**query**  <br>*optional*|string|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|
|**Query**|**visible**  <br>*optional*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-menu-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="rolemenutreeselectusingget"></a>
### roleMenuTreeselect
```
GET /menu/roleMenuTreeselect/{roleId}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**roleId**  <br>*required*|roleId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-menu-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="treeselectusingget"></a>
### treeselect
```
GET /menu/treeselect
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**component**  <br>*optional*|string|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**icon**  <br>*optional*|string|
|**Query**|**isCache**  <br>*optional*|string|
|**Query**|**isFrame**  <br>*optional*|string|
|**Query**|**menuId**  <br>*optional*|integer (int64)|
|**Query**|**menuName**  <br>*optional*|string|
|**Query**|**menuType**  <br>*optional*|string|
|**Query**|**orderNum**  <br>*optional*|integer (int32)|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**parentId**  <br>*optional*|integer (int64)|
|**Query**|**parentName**  <br>*optional*|string|
|**Query**|**path**  <br>*optional*|string|
|**Query**|**perms**  <br>*optional*|string|
|**Query**|**query**  <br>*optional*|string|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|
|**Query**|**visible**  <br>*optional*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-menu-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="getinfousingget_4"></a>
### getInfo
```
GET /menu/{menuId}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**menuId**  <br>*required*|menuId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-menu-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="removeusingdelete_5"></a>
### remove
```
DELETE /menu/{menuId}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**menuId**  <br>*required*|menuId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-menu-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="addusingpost_6"></a>
### add
```
POST /notice
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**notice**  <br>*required*|notice|[SysNotice](#sysnotice)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-notice-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="editusingput_5"></a>
### edit
```
PUT /notice
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**notice**  <br>*required*|notice|[SysNotice](#sysnotice)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-notice-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="listusingget_6"></a>
### list
```
GET /notice/list
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**noticeContent**  <br>*optional*|string|
|**Query**|**noticeId**  <br>*optional*|integer (int64)|
|**Query**|**noticeTitle**  <br>*optional*|string|
|**Query**|**noticeType**  <br>*optional*|string|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TableDataInfo](#tabledatainfo)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-notice-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="removeusingdelete_6"></a>
### remove
```
DELETE /notice/{noticeIds}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**noticeIds**  <br>*required*|noticeIds|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-notice-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="getinfousingget_5"></a>
### getInfo
```
GET /notice/{noticeId}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**noticeId**  <br>*required*|noticeId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-notice-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="listusingget_11"></a>
### list
```
GET /online/list
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**ipaddr**  <br>*optional*|ipaddr|string|
|**Query**|**userName**  <br>*optional*|userName|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TableDataInfo](#tabledatainfo)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-user-online-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="forcelogoutusingdelete"></a>
### forceLogout
```
DELETE /online/{tokenId}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**tokenId**  <br>*required*|tokenId|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-user-online-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="addusingpost_7"></a>
### add
```
POST /operlog
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**operLog**  <br>*required*|operLog|[SysOperLog](#sysoperlog)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-operlog-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="cleanusingdelete_1"></a>
### clean
```
DELETE /operlog/clean
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-operlog-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="exportusingpost_4"></a>
### export
```
POST /operlog/export
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**businessType**  <br>*optional*|integer (int32)|
|**Query**|**businessTypes**  <br>*optional*|< integer (int32) > array(multi)|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**deptName**  <br>*optional*|string|
|**Query**|**errorMsg**  <br>*optional*|string|
|**Query**|**jsonResult**  <br>*optional*|string|
|**Query**|**method**  <br>*optional*|string|
|**Query**|**operId**  <br>*optional*|integer (int64)|
|**Query**|**operIp**  <br>*optional*|string|
|**Query**|**operName**  <br>*optional*|string|
|**Query**|**operParam**  <br>*optional*|string|
|**Query**|**operTime**  <br>*optional*|string (date-time)|
|**Query**|**operUrl**  <br>*optional*|string|
|**Query**|**operatorType**  <br>*optional*|integer (int32)|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**requestMethod**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|integer (int32)|
|**Query**|**title**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|No Content|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-operlog-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="listusingget_7"></a>
### list
```
GET /operlog/list
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**businessType**  <br>*optional*|integer (int32)|
|**Query**|**businessTypes**  <br>*optional*|< integer (int32) > array(multi)|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**deptName**  <br>*optional*|string|
|**Query**|**errorMsg**  <br>*optional*|string|
|**Query**|**jsonResult**  <br>*optional*|string|
|**Query**|**method**  <br>*optional*|string|
|**Query**|**operId**  <br>*optional*|integer (int64)|
|**Query**|**operIp**  <br>*optional*|string|
|**Query**|**operName**  <br>*optional*|string|
|**Query**|**operParam**  <br>*optional*|string|
|**Query**|**operTime**  <br>*optional*|string (date-time)|
|**Query**|**operUrl**  <br>*optional*|string|
|**Query**|**operatorType**  <br>*optional*|integer (int32)|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**requestMethod**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|integer (int32)|
|**Query**|**title**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TableDataInfo](#tabledatainfo)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-operlog-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="removeusingdelete_7"></a>
### remove
```
DELETE /operlog/{operIds}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**operIds**  <br>*required*|operIds|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-operlog-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="addusingpost_8"></a>
### add
```
POST /post
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**post**  <br>*required*|post|[SysPost](#syspost)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-post-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="editusingput_6"></a>
### edit
```
PUT /post
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**post**  <br>*required*|post|[SysPost](#syspost)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-post-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="exportusingpost_5"></a>
### export
```
POST /post/export
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**flag**  <br>*optional*|boolean|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**postCode**  <br>*optional*|string|
|**Query**|**postId**  <br>*optional*|integer (int64)|
|**Query**|**postName**  <br>*optional*|string|
|**Query**|**postSort**  <br>*optional*|integer (int32)|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|No Content|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-post-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="listusingget_8"></a>
### list
```
GET /post/list
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**flag**  <br>*optional*|boolean|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**postCode**  <br>*optional*|string|
|**Query**|**postId**  <br>*optional*|integer (int64)|
|**Query**|**postName**  <br>*optional*|string|
|**Query**|**postSort**  <br>*optional*|integer (int32)|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TableDataInfo](#tabledatainfo)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-post-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="optionselectusingget_1"></a>
### optionselect
```
GET /post/optionselect
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-post-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="removeusingdelete_8"></a>
### remove
```
DELETE /post/{postIds}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**postIds**  <br>*required*|postIds|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-post-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="getinfousingget_6"></a>
### getInfo
```
GET /post/{postId}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**postId**  <br>*required*|postId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-post-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="addusingpost_9"></a>
### add
```
POST /role
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**role**  <br>*required*|role|[SysRole](#sysrole)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-role-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="editusingput_7"></a>
### edit
```
PUT /role
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**role**  <br>*required*|role|[SysRole](#sysrole)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-role-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="allocatedlistusingget"></a>
### allocatedList
```
GET /role/authUser/allocatedList
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**admin**  <br>*optional*|boolean|
|**Query**|**avatar**  <br>*optional*|string|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**delFlag**  <br>*optional*|string|
|**Query**|**dept.ancestors**  <br>*optional*|string|
|**Query**|**dept.createBy**  <br>*optional*|string|
|**Query**|**dept.createTime**  <br>*optional*|string (date-time)|
|**Query**|**dept.delFlag**  <br>*optional*|string|
|**Query**|**dept.deptId**  <br>*optional*|integer (int64)|
|**Query**|**dept.deptName**  <br>*optional*|string|
|**Query**|**dept.email**  <br>*optional*|string|
|**Query**|**dept.leader**  <br>*optional*|string|
|**Query**|**dept.orderNum**  <br>*optional*|integer (int32)|
|**Query**|**dept.params**  <br>*optional*|object|
|**Query**|**dept.parentId**  <br>*optional*|integer (int64)|
|**Query**|**dept.parentName**  <br>*optional*|string|
|**Query**|**dept.phone**  <br>*optional*|string|
|**Query**|**dept.remark**  <br>*optional*|string|
|**Query**|**dept.searchValue**  <br>*optional*|string|
|**Query**|**dept.status**  <br>*optional*|string|
|**Query**|**dept.updateBy**  <br>*optional*|string|
|**Query**|**dept.updateTime**  <br>*optional*|string (date-time)|
|**Query**|**deptId**  <br>*optional*|integer (int64)|
|**Query**|**email**  <br>*optional*|string|
|**Query**|**loginDate**  <br>*optional*|string (date-time)|
|**Query**|**loginIp**  <br>*optional*|string|
|**Query**|**nickName**  <br>*optional*|string|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**password**  <br>*optional*|string|
|**Query**|**phonenumber**  <br>*optional*|string|
|**Query**|**postIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**roleId**  <br>*optional*|integer (int64)|
|**Query**|**roleIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**roles[0].admin**  <br>*optional*|boolean|
|**Query**|**roles[0].createBy**  <br>*optional*|string|
|**Query**|**roles[0].createTime**  <br>*optional*|string (date-time)|
|**Query**|**roles[0].dataScope**  <br>*optional*|string|
|**Query**|**roles[0].delFlag**  <br>*optional*|string|
|**Query**|**roles[0].deptCheckStrictly**  <br>*optional*|boolean|
|**Query**|**roles[0].deptIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**roles[0].flag**  <br>*optional*|boolean|
|**Query**|**roles[0].menuCheckStrictly**  <br>*optional*|boolean|
|**Query**|**roles[0].menuIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**roles[0].params**  <br>*optional*|object|
|**Query**|**roles[0].permissions**  <br>*optional*|< string > array(multi)|
|**Query**|**roles[0].remark**  <br>*optional*|string|
|**Query**|**roles[0].roleId**  <br>*optional*|integer (int64)|
|**Query**|**roles[0].roleKey**  <br>*optional*|string|
|**Query**|**roles[0].roleName**  <br>*optional*|string|
|**Query**|**roles[0].roleSort**  <br>*optional*|integer (int32)|
|**Query**|**roles[0].searchValue**  <br>*optional*|string|
|**Query**|**roles[0].status**  <br>*optional*|string|
|**Query**|**roles[0].updateBy**  <br>*optional*|string|
|**Query**|**roles[0].updateTime**  <br>*optional*|string (date-time)|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**sex**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|
|**Query**|**userId**  <br>*optional*|integer (int64)|
|**Query**|**userName**  <br>*optional*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TableDataInfo](#tabledatainfo)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-role-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="cancelauthuserusingput"></a>
### cancelAuthUser
```
PUT /role/authUser/cancel
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**userRole**  <br>*required*|userRole|[SysUserRole](#sysuserrole)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-role-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="cancelauthuserallusingput"></a>
### cancelAuthUserAll
```
PUT /role/authUser/cancelAll
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**roleId**  <br>*optional*|roleId|integer (int64)|
|**Query**|**userIds**  <br>*optional*|userIds|< integer (int64) > array(multi)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-role-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="selectauthuserallusingput"></a>
### selectAuthUserAll
```
PUT /role/authUser/selectAll
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**roleId**  <br>*optional*|roleId|integer (int64)|
|**Query**|**userIds**  <br>*optional*|userIds|< integer (int64) > array(multi)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-role-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="unallocatedlistusingget"></a>
### unallocatedList
```
GET /role/authUser/unallocatedList
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**admin**  <br>*optional*|boolean|
|**Query**|**avatar**  <br>*optional*|string|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**delFlag**  <br>*optional*|string|
|**Query**|**dept.ancestors**  <br>*optional*|string|
|**Query**|**dept.createBy**  <br>*optional*|string|
|**Query**|**dept.createTime**  <br>*optional*|string (date-time)|
|**Query**|**dept.delFlag**  <br>*optional*|string|
|**Query**|**dept.deptId**  <br>*optional*|integer (int64)|
|**Query**|**dept.deptName**  <br>*optional*|string|
|**Query**|**dept.email**  <br>*optional*|string|
|**Query**|**dept.leader**  <br>*optional*|string|
|**Query**|**dept.orderNum**  <br>*optional*|integer (int32)|
|**Query**|**dept.params**  <br>*optional*|object|
|**Query**|**dept.parentId**  <br>*optional*|integer (int64)|
|**Query**|**dept.parentName**  <br>*optional*|string|
|**Query**|**dept.phone**  <br>*optional*|string|
|**Query**|**dept.remark**  <br>*optional*|string|
|**Query**|**dept.searchValue**  <br>*optional*|string|
|**Query**|**dept.status**  <br>*optional*|string|
|**Query**|**dept.updateBy**  <br>*optional*|string|
|**Query**|**dept.updateTime**  <br>*optional*|string (date-time)|
|**Query**|**deptId**  <br>*optional*|integer (int64)|
|**Query**|**email**  <br>*optional*|string|
|**Query**|**loginDate**  <br>*optional*|string (date-time)|
|**Query**|**loginIp**  <br>*optional*|string|
|**Query**|**nickName**  <br>*optional*|string|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**password**  <br>*optional*|string|
|**Query**|**phonenumber**  <br>*optional*|string|
|**Query**|**postIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**roleId**  <br>*optional*|integer (int64)|
|**Query**|**roleIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**roles[0].admin**  <br>*optional*|boolean|
|**Query**|**roles[0].createBy**  <br>*optional*|string|
|**Query**|**roles[0].createTime**  <br>*optional*|string (date-time)|
|**Query**|**roles[0].dataScope**  <br>*optional*|string|
|**Query**|**roles[0].delFlag**  <br>*optional*|string|
|**Query**|**roles[0].deptCheckStrictly**  <br>*optional*|boolean|
|**Query**|**roles[0].deptIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**roles[0].flag**  <br>*optional*|boolean|
|**Query**|**roles[0].menuCheckStrictly**  <br>*optional*|boolean|
|**Query**|**roles[0].menuIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**roles[0].params**  <br>*optional*|object|
|**Query**|**roles[0].permissions**  <br>*optional*|< string > array(multi)|
|**Query**|**roles[0].remark**  <br>*optional*|string|
|**Query**|**roles[0].roleId**  <br>*optional*|integer (int64)|
|**Query**|**roles[0].roleKey**  <br>*optional*|string|
|**Query**|**roles[0].roleName**  <br>*optional*|string|
|**Query**|**roles[0].roleSort**  <br>*optional*|integer (int32)|
|**Query**|**roles[0].searchValue**  <br>*optional*|string|
|**Query**|**roles[0].status**  <br>*optional*|string|
|**Query**|**roles[0].updateBy**  <br>*optional*|string|
|**Query**|**roles[0].updateTime**  <br>*optional*|string (date-time)|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**sex**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|
|**Query**|**userId**  <br>*optional*|integer (int64)|
|**Query**|**userName**  <br>*optional*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TableDataInfo](#tabledatainfo)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-role-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="changestatususingput"></a>
### changeStatus
```
PUT /role/changeStatus
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**role**  <br>*required*|role|[SysRole](#sysrole)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-role-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="datascopeusingput"></a>
### dataScope
```
PUT /role/dataScope
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**role**  <br>*required*|role|[SysRole](#sysrole)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-role-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="depttreeusingget"></a>
### deptTree
```
GET /role/deptTree/{roleId}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**roleId**  <br>*required*|roleId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-role-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="exportusingpost_6"></a>
### export
```
POST /role/export
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**admin**  <br>*optional*|boolean|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**dataScope**  <br>*optional*|string|
|**Query**|**delFlag**  <br>*optional*|string|
|**Query**|**deptCheckStrictly**  <br>*optional*|boolean|
|**Query**|**deptIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**flag**  <br>*optional*|boolean|
|**Query**|**menuCheckStrictly**  <br>*optional*|boolean|
|**Query**|**menuIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**permissions**  <br>*optional*|< string > array(multi)|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**roleId**  <br>*optional*|integer (int64)|
|**Query**|**roleKey**  <br>*optional*|string|
|**Query**|**roleName**  <br>*optional*|string|
|**Query**|**roleSort**  <br>*optional*|integer (int32)|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|No Content|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-role-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="listusingget_9"></a>
### list
```
GET /role/list
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**admin**  <br>*optional*|boolean|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**dataScope**  <br>*optional*|string|
|**Query**|**delFlag**  <br>*optional*|string|
|**Query**|**deptCheckStrictly**  <br>*optional*|boolean|
|**Query**|**deptIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**flag**  <br>*optional*|boolean|
|**Query**|**menuCheckStrictly**  <br>*optional*|boolean|
|**Query**|**menuIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**permissions**  <br>*optional*|< string > array(multi)|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**roleId**  <br>*optional*|integer (int64)|
|**Query**|**roleKey**  <br>*optional*|string|
|**Query**|**roleName**  <br>*optional*|string|
|**Query**|**roleSort**  <br>*optional*|integer (int32)|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TableDataInfo](#tabledatainfo)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-role-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="optionselectusingget_2"></a>
### optionselect
```
GET /role/optionselect
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-role-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="removeusingdelete_9"></a>
### remove
```
DELETE /role/{roleIds}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**roleIds**  <br>*required*|roleIds|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-role-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="getinfousingget_7"></a>
### getInfo
```
GET /role/{roleId}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**roleId**  <br>*required*|roleId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-role-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="addusingpost_10"></a>
### add
```
POST /user
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**user**  <br>*required*|user|[SysUserReq](#sysuserreq)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="editusingput_8"></a>
### edit
```
PUT /user
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**user**  <br>*required*|user|[SysUserReq](#sysuserreq)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="getinfousingget_9"></a>
### getInfo
```
GET /user/
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**userId**  <br>*required*|userId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="insertauthroleusingput"></a>
### insertAuthRole
```
PUT /user/authRole
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**roleIds**  <br>*optional*|roleIds|< integer (int64) > array(multi)|
|**Query**|**userId**  <br>*optional*|userId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="authroleusingget"></a>
### authRole
```
GET /user/authRole/{userId}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**userId**  <br>*required*|userId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="changestatususingput_1"></a>
### changeStatus
```
PUT /user/changeStatus
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**user**  <br>*required*|user|[SysUserReq](#sysuserreq)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="depttreeusingget_1"></a>
### deptTree
```
GET /user/deptTree
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**ancestors**  <br>*optional*|string|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**delFlag**  <br>*optional*|string|
|**Query**|**deptId**  <br>*optional*|integer (int64)|
|**Query**|**deptName**  <br>*optional*|string|
|**Query**|**email**  <br>*optional*|string|
|**Query**|**leader**  <br>*optional*|string|
|**Query**|**orderNum**  <br>*optional*|integer (int32)|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**parentId**  <br>*optional*|integer (int64)|
|**Query**|**parentName**  <br>*optional*|string|
|**Query**|**phone**  <br>*optional*|string|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="exportusingpost_7"></a>
### export
```
POST /user/export
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**admin**  <br>*optional*|boolean|
|**Query**|**avatar**  <br>*optional*|string|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**delFlag**  <br>*optional*|string|
|**Query**|**dept.ancestors**  <br>*optional*|string|
|**Query**|**dept.createBy**  <br>*optional*|string|
|**Query**|**dept.createTime**  <br>*optional*|string (date-time)|
|**Query**|**dept.delFlag**  <br>*optional*|string|
|**Query**|**dept.deptId**  <br>*optional*|integer (int64)|
|**Query**|**dept.deptName**  <br>*optional*|string|
|**Query**|**dept.email**  <br>*optional*|string|
|**Query**|**dept.leader**  <br>*optional*|string|
|**Query**|**dept.orderNum**  <br>*optional*|integer (int32)|
|**Query**|**dept.params**  <br>*optional*|object|
|**Query**|**dept.parentId**  <br>*optional*|integer (int64)|
|**Query**|**dept.parentName**  <br>*optional*|string|
|**Query**|**dept.phone**  <br>*optional*|string|
|**Query**|**dept.remark**  <br>*optional*|string|
|**Query**|**dept.searchValue**  <br>*optional*|string|
|**Query**|**dept.status**  <br>*optional*|string|
|**Query**|**dept.updateBy**  <br>*optional*|string|
|**Query**|**dept.updateTime**  <br>*optional*|string (date-time)|
|**Query**|**deptId**  <br>*optional*|integer (int64)|
|**Query**|**email**  <br>*optional*|string|
|**Query**|**loginDate**  <br>*optional*|string (date-time)|
|**Query**|**loginIp**  <br>*optional*|string|
|**Query**|**nickName**  <br>*optional*|string|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**password**  <br>*optional*|string|
|**Query**|**phonenumber**  <br>*optional*|string|
|**Query**|**postIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**roleId**  <br>*optional*|integer (int64)|
|**Query**|**roleIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**roles[0].admin**  <br>*optional*|boolean|
|**Query**|**roles[0].createBy**  <br>*optional*|string|
|**Query**|**roles[0].createTime**  <br>*optional*|string (date-time)|
|**Query**|**roles[0].dataScope**  <br>*optional*|string|
|**Query**|**roles[0].delFlag**  <br>*optional*|string|
|**Query**|**roles[0].deptCheckStrictly**  <br>*optional*|boolean|
|**Query**|**roles[0].deptIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**roles[0].flag**  <br>*optional*|boolean|
|**Query**|**roles[0].menuCheckStrictly**  <br>*optional*|boolean|
|**Query**|**roles[0].menuIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**roles[0].params**  <br>*optional*|object|
|**Query**|**roles[0].permissions**  <br>*optional*|< string > array(multi)|
|**Query**|**roles[0].remark**  <br>*optional*|string|
|**Query**|**roles[0].roleId**  <br>*optional*|integer (int64)|
|**Query**|**roles[0].roleKey**  <br>*optional*|string|
|**Query**|**roles[0].roleName**  <br>*optional*|string|
|**Query**|**roles[0].roleSort**  <br>*optional*|integer (int32)|
|**Query**|**roles[0].searchValue**  <br>*optional*|string|
|**Query**|**roles[0].status**  <br>*optional*|string|
|**Query**|**roles[0].updateBy**  <br>*optional*|string|
|**Query**|**roles[0].updateTime**  <br>*optional*|string (date-time)|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**sex**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|
|**Query**|**userId**  <br>*optional*|integer (int64)|
|**Query**|**userName**  <br>*optional*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|No Content|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="getinfousingget_8"></a>
### getInfo
```
GET /user/getInfo
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="importdatausingpost"></a>
### importData
```
POST /user/importData
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**updateSupport**  <br>*optional*|updateSupport|boolean|
|**Body**|**file**  <br>*optional*|file|string (binary)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `multipart/form-data`


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="importtemplateusingpost"></a>
### importTemplate
```
POST /user/importTemplate
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|No Content|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="infousingget"></a>
### info
```
GET /user/info/{username}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**username**  <br>*required*|username|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[R«LoginUser»](#b3a1a9803a237947c0b760bee7fb0d22)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="listusingget_10"></a>
### list
```
GET /user/list
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**admin**  <br>*optional*|boolean|
|**Query**|**avatar**  <br>*optional*|string|
|**Query**|**createBy**  <br>*optional*|string|
|**Query**|**createTime**  <br>*optional*|string (date-time)|
|**Query**|**delFlag**  <br>*optional*|string|
|**Query**|**dept.ancestors**  <br>*optional*|string|
|**Query**|**dept.createBy**  <br>*optional*|string|
|**Query**|**dept.createTime**  <br>*optional*|string (date-time)|
|**Query**|**dept.delFlag**  <br>*optional*|string|
|**Query**|**dept.deptId**  <br>*optional*|integer (int64)|
|**Query**|**dept.deptName**  <br>*optional*|string|
|**Query**|**dept.email**  <br>*optional*|string|
|**Query**|**dept.leader**  <br>*optional*|string|
|**Query**|**dept.orderNum**  <br>*optional*|integer (int32)|
|**Query**|**dept.params**  <br>*optional*|object|
|**Query**|**dept.parentId**  <br>*optional*|integer (int64)|
|**Query**|**dept.parentName**  <br>*optional*|string|
|**Query**|**dept.phone**  <br>*optional*|string|
|**Query**|**dept.remark**  <br>*optional*|string|
|**Query**|**dept.searchValue**  <br>*optional*|string|
|**Query**|**dept.status**  <br>*optional*|string|
|**Query**|**dept.updateBy**  <br>*optional*|string|
|**Query**|**dept.updateTime**  <br>*optional*|string (date-time)|
|**Query**|**deptId**  <br>*optional*|integer (int64)|
|**Query**|**email**  <br>*optional*|string|
|**Query**|**loginDate**  <br>*optional*|string (date-time)|
|**Query**|**loginIp**  <br>*optional*|string|
|**Query**|**nickName**  <br>*optional*|string|
|**Query**|**params**  <br>*optional*|object|
|**Query**|**password**  <br>*optional*|string|
|**Query**|**phonenumber**  <br>*optional*|string|
|**Query**|**postIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**remark**  <br>*optional*|string|
|**Query**|**roleId**  <br>*optional*|integer (int64)|
|**Query**|**roleIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**roles[0].admin**  <br>*optional*|boolean|
|**Query**|**roles[0].createBy**  <br>*optional*|string|
|**Query**|**roles[0].createTime**  <br>*optional*|string (date-time)|
|**Query**|**roles[0].dataScope**  <br>*optional*|string|
|**Query**|**roles[0].delFlag**  <br>*optional*|string|
|**Query**|**roles[0].deptCheckStrictly**  <br>*optional*|boolean|
|**Query**|**roles[0].deptIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**roles[0].flag**  <br>*optional*|boolean|
|**Query**|**roles[0].menuCheckStrictly**  <br>*optional*|boolean|
|**Query**|**roles[0].menuIds**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**roles[0].params**  <br>*optional*|object|
|**Query**|**roles[0].permissions**  <br>*optional*|< string > array(multi)|
|**Query**|**roles[0].remark**  <br>*optional*|string|
|**Query**|**roles[0].roleId**  <br>*optional*|integer (int64)|
|**Query**|**roles[0].roleKey**  <br>*optional*|string|
|**Query**|**roles[0].roleName**  <br>*optional*|string|
|**Query**|**roles[0].roleSort**  <br>*optional*|integer (int32)|
|**Query**|**roles[0].searchValue**  <br>*optional*|string|
|**Query**|**roles[0].status**  <br>*optional*|string|
|**Query**|**roles[0].updateBy**  <br>*optional*|string|
|**Query**|**roles[0].updateTime**  <br>*optional*|string (date-time)|
|**Query**|**searchValue**  <br>*optional*|string|
|**Query**|**sex**  <br>*optional*|string|
|**Query**|**status**  <br>*optional*|string|
|**Query**|**updateBy**  <br>*optional*|string|
|**Query**|**updateTime**  <br>*optional*|string (date-time)|
|**Query**|**userId**  <br>*optional*|integer (int64)|
|**Query**|**userName**  <br>*optional*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TableDataInfo](#tabledatainfo)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="profileusingget"></a>
### profile
```
GET /user/profile
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-profile-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="updateprofileusingput"></a>
### updateProfile
```
PUT /user/profile
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**user**  <br>*required*|user|[SysUser](#sysuser)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-profile-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="avatarusingpost"></a>
### avatar
```
POST /user/profile/avatar
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**avatarfile**  <br>*required*|avatarfile|string (binary)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `multipart/form-data`


#### Produces

* `*/*`


#### Tags

* sys-profile-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="updatepwdusingput"></a>
### updatePwd
```
PUT /user/profile/updatePwd
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**newPassword**  <br>*optional*|newPassword|string|
|**Query**|**oldPassword**  <br>*optional*|oldPassword|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-profile-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="registerusingpost"></a>
### register
```
POST /user/register
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**sysUser**  <br>*required*|sysUser|[SysUserReq](#sysuserreq)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[R«boolean»](#ca50889f5231467f9d5aa71716744e52)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="resetpwdusingput"></a>
### resetPwd
```
PUT /user/resetPwd
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**user**  <br>*required*|user|[SysUserReq](#sysuserreq)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="removeusingdelete_10"></a>
### remove
```
DELETE /user/{userIds}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**userIds**  <br>*required*|userIds|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**204**|No Content|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="getinfousingget_10"></a>
### getInfo
```
GET /user/{userId}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**userId**  <br>*required*|userId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|< string, object > map|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* sys-user-controller


#### Security

|Type|Name|Scopes|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|




<a name="definitions"></a>
## Definitions

<a name="loginuser"></a>
### LoginUser

|Name|Schema|
|---|---|
|**expireTime**  <br>*optional*|integer (int64)|
|**ipaddr**  <br>*optional*|string|
|**loginTime**  <br>*optional*|integer (int64)|
|**permissions**  <br>*optional*|< string > array|
|**roles**  <br>*optional*|< string > array|
|**sysUser**  <br>*optional*|[SysUserRes](#sysuserres)|
|**token**  <br>*optional*|string|
|**userid**  <br>*optional*|integer (int64)|
|**username**  <br>*optional*|string|


<a name="b3a1a9803a237947c0b760bee7fb0d22"></a>
### R«LoginUser»

|Name|Schema|
|---|---|
|**code**  <br>*optional*|integer (int32)|
|**data**  <br>*optional*|[LoginUser](#loginuser)|
|**msg**  <br>*optional*|string|


<a name="ca50889f5231467f9d5aa71716744e52"></a>
### R«boolean»

|Name|Schema|
|---|---|
|**code**  <br>*optional*|integer (int32)|
|**data**  <br>*optional*|boolean|
|**msg**  <br>*optional*|string|


<a name="sysconfig"></a>
### SysConfig

|Name|Schema|
|---|---|
|**configId**  <br>*optional*|integer (int64)|
|**configKey**  <br>*optional*|string|
|**configName**  <br>*optional*|string|
|**configType**  <br>*optional*|string|
|**configValue**  <br>*optional*|string|
|**createBy**  <br>*optional*|string|
|**createTime**  <br>*optional*|string (date-time)|
|**params**  <br>*optional*|object|
|**remark**  <br>*optional*|string|
|**updateBy**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|


<a name="sysdept"></a>
### SysDept

|Name|Schema|
|---|---|
|**ancestors**  <br>*optional*|string|
|**children**  <br>*optional*|< [SysDept](#sysdept) > array|
|**createBy**  <br>*optional*|string|
|**createTime**  <br>*optional*|string (date-time)|
|**delFlag**  <br>*optional*|string|
|**deptId**  <br>*optional*|integer (int64)|
|**deptName**  <br>*optional*|string|
|**email**  <br>*optional*|string|
|**leader**  <br>*optional*|string|
|**orderNum**  <br>*optional*|integer (int32)|
|**params**  <br>*optional*|object|
|**parentId**  <br>*optional*|integer (int64)|
|**parentName**  <br>*optional*|string|
|**phone**  <br>*optional*|string|
|**remark**  <br>*optional*|string|
|**status**  <br>*optional*|string|
|**updateBy**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|


<a name="sysdictdata"></a>
### SysDictData

|Name|Schema|
|---|---|
|**createBy**  <br>*optional*|string|
|**createTime**  <br>*optional*|string (date-time)|
|**cssClass**  <br>*optional*|string|
|**dictCode**  <br>*optional*|integer (int64)|
|**dictLabel**  <br>*optional*|string|
|**dictSort**  <br>*optional*|integer (int64)|
|**dictType**  <br>*optional*|string|
|**dictValue**  <br>*optional*|string|
|**isDefault**  <br>*optional*|string|
|**listClass**  <br>*optional*|string|
|**params**  <br>*optional*|object|
|**remark**  <br>*optional*|string|
|**status**  <br>*optional*|string|
|**updateBy**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|


<a name="sysdicttype"></a>
### SysDictType

|Name|Schema|
|---|---|
|**createBy**  <br>*optional*|string|
|**createTime**  <br>*optional*|string (date-time)|
|**dictId**  <br>*optional*|integer (int64)|
|**dictName**  <br>*optional*|string|
|**dictType**  <br>*optional*|string|
|**params**  <br>*optional*|object|
|**remark**  <br>*optional*|string|
|**status**  <br>*optional*|string|
|**updateBy**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|


<a name="syslogininfor"></a>
### SysLogininfor

|Name|Schema|
|---|---|
|**accessTime**  <br>*optional*|string (date-time)|
|**createBy**  <br>*optional*|string|
|**createTime**  <br>*optional*|string (date-time)|
|**infoId**  <br>*optional*|integer (int64)|
|**ipaddr**  <br>*optional*|string|
|**msg**  <br>*optional*|string|
|**params**  <br>*optional*|object|
|**remark**  <br>*optional*|string|
|**status**  <br>*optional*|string|
|**updateBy**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|
|**userName**  <br>*optional*|string|


<a name="sysmenu"></a>
### SysMenu

|Name|Schema|
|---|---|
|**children**  <br>*optional*|< [SysMenu](#sysmenu) > array|
|**component**  <br>*optional*|string|
|**createBy**  <br>*optional*|string|
|**createTime**  <br>*optional*|string (date-time)|
|**icon**  <br>*optional*|string|
|**isCache**  <br>*optional*|string|
|**isFrame**  <br>*optional*|string|
|**menuId**  <br>*optional*|integer (int64)|
|**menuName**  <br>*optional*|string|
|**menuType**  <br>*optional*|string|
|**orderNum**  <br>*optional*|integer (int32)|
|**params**  <br>*optional*|object|
|**parentId**  <br>*optional*|integer (int64)|
|**parentName**  <br>*optional*|string|
|**path**  <br>*optional*|string|
|**perms**  <br>*optional*|string|
|**query**  <br>*optional*|string|
|**remark**  <br>*optional*|string|
|**status**  <br>*optional*|string|
|**updateBy**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|
|**visible**  <br>*optional*|string|


<a name="sysnotice"></a>
### SysNotice

|Name|Schema|
|---|---|
|**createBy**  <br>*optional*|string|
|**createTime**  <br>*optional*|string (date-time)|
|**noticeContent**  <br>*optional*|string|
|**noticeId**  <br>*optional*|integer (int64)|
|**noticeTitle**  <br>*optional*|string|
|**noticeType**  <br>*optional*|string|
|**params**  <br>*optional*|object|
|**remark**  <br>*optional*|string|
|**status**  <br>*optional*|string|
|**updateBy**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|


<a name="sysoperlog"></a>
### SysOperLog

|Name|Schema|
|---|---|
|**businessType**  <br>*optional*|integer (int32)|
|**businessTypes**  <br>*optional*|< integer (int32) > array|
|**createBy**  <br>*optional*|string|
|**createTime**  <br>*optional*|string (date-time)|
|**deptName**  <br>*optional*|string|
|**errorMsg**  <br>*optional*|string|
|**jsonResult**  <br>*optional*|string|
|**method**  <br>*optional*|string|
|**operId**  <br>*optional*|integer (int64)|
|**operIp**  <br>*optional*|string|
|**operName**  <br>*optional*|string|
|**operParam**  <br>*optional*|string|
|**operTime**  <br>*optional*|string (date-time)|
|**operUrl**  <br>*optional*|string|
|**operatorType**  <br>*optional*|integer (int32)|
|**params**  <br>*optional*|object|
|**remark**  <br>*optional*|string|
|**requestMethod**  <br>*optional*|string|
|**status**  <br>*optional*|integer (int32)|
|**title**  <br>*optional*|string|
|**updateBy**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|


<a name="syspost"></a>
### SysPost

|Name|Schema|
|---|---|
|**createBy**  <br>*optional*|string|
|**createTime**  <br>*optional*|string (date-time)|
|**flag**  <br>*optional*|boolean|
|**params**  <br>*optional*|object|
|**postCode**  <br>*optional*|string|
|**postId**  <br>*optional*|integer (int64)|
|**postName**  <br>*optional*|string|
|**postSort**  <br>*optional*|integer (int32)|
|**remark**  <br>*optional*|string|
|**status**  <br>*optional*|string|
|**updateBy**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|


<a name="sysrole"></a>
### SysRole

|Name|Schema|
|---|---|
|**createBy**  <br>*optional*|string|
|**createTime**  <br>*optional*|string (date-time)|
|**dataScope**  <br>*optional*|string|
|**delFlag**  <br>*optional*|string|
|**deptCheckStrictly**  <br>*optional*|boolean|
|**deptIds**  <br>*optional*|< integer (int64) > array|
|**flag**  <br>*optional*|boolean|
|**menuCheckStrictly**  <br>*optional*|boolean|
|**menuIds**  <br>*optional*|< integer (int64) > array|
|**params**  <br>*optional*|object|
|**permissions**  <br>*optional*|< string > array|
|**remark**  <br>*optional*|string|
|**roleId**  <br>*optional*|integer (int64)|
|**roleKey**  <br>*optional*|string|
|**roleName**  <br>*optional*|string|
|**roleSort**  <br>*optional*|integer (int32)|
|**status**  <br>*optional*|string|
|**updateBy**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|


<a name="sysrolereq"></a>
### SysRoleReq

|Name|Schema|
|---|---|
|**createBy**  <br>*optional*|string|
|**createTime**  <br>*optional*|string (date-time)|
|**dataScope**  <br>*optional*|string|
|**delFlag**  <br>*optional*|string|
|**deptCheckStrictly**  <br>*optional*|boolean|
|**deptIds**  <br>*optional*|< integer (int64) > array|
|**flag**  <br>*optional*|boolean|
|**menuCheckStrictly**  <br>*optional*|boolean|
|**menuIds**  <br>*optional*|< integer (int64) > array|
|**params**  <br>*optional*|object|
|**permissions**  <br>*optional*|< string > array|
|**remark**  <br>*optional*|string|
|**roleId**  <br>*optional*|integer (int64)|
|**roleKey**  <br>*optional*|string|
|**roleName**  <br>*optional*|string|
|**roleSort**  <br>*optional*|integer (int32)|
|**status**  <br>*optional*|string|
|**updateBy**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|


<a name="sysroleres"></a>
### SysRoleRes

|Name|Schema|
|---|---|
|**admin**  <br>*optional*|boolean|
|**createBy**  <br>*optional*|string|
|**createTime**  <br>*optional*|string (date-time)|
|**dataScope**  <br>*optional*|string|
|**delFlag**  <br>*optional*|string|
|**deptCheckStrictly**  <br>*optional*|boolean|
|**deptIds**  <br>*optional*|< integer (int64) > array|
|**flag**  <br>*optional*|boolean|
|**menuCheckStrictly**  <br>*optional*|boolean|
|**menuIds**  <br>*optional*|< integer (int64) > array|
|**params**  <br>*optional*|object|
|**permissions**  <br>*optional*|< string > array|
|**remark**  <br>*optional*|string|
|**roleId**  <br>*optional*|integer (int64)|
|**roleKey**  <br>*optional*|string|
|**roleName**  <br>*optional*|string|
|**roleSort**  <br>*optional*|integer (int32)|
|**status**  <br>*optional*|string|
|**updateBy**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|


<a name="sysuser"></a>
### SysUser

|Name|Schema|
|---|---|
|**avatar**  <br>*optional*|string|
|**createBy**  <br>*optional*|string|
|**createTime**  <br>*optional*|string (date-time)|
|**delFlag**  <br>*optional*|string|
|**dept**  <br>*optional*|[SysDept](#sysdept)|
|**deptId**  <br>*optional*|integer (int64)|
|**email**  <br>*optional*|string|
|**loginDate**  <br>*optional*|string (date-time)|
|**loginIp**  <br>*optional*|string|
|**nickName**  <br>*optional*|string|
|**params**  <br>*optional*|object|
|**password**  <br>*optional*|string|
|**phonenumber**  <br>*optional*|string|
|**postIds**  <br>*optional*|< integer (int64) > array|
|**remark**  <br>*optional*|string|
|**roleId**  <br>*optional*|integer (int64)|
|**roleIds**  <br>*optional*|< integer (int64) > array|
|**roles**  <br>*optional*|< [SysRole](#sysrole) > array|
|**sex**  <br>*optional*|string|
|**status**  <br>*optional*|string|
|**updateBy**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|
|**userId**  <br>*optional*|integer (int64)|
|**userName**  <br>*optional*|string|


<a name="sysuserreq"></a>
### SysUserReq

|Name|Schema|
|---|---|
|**avatar**  <br>*optional*|string|
|**createBy**  <br>*optional*|string|
|**createTime**  <br>*optional*|string (date-time)|
|**delFlag**  <br>*optional*|string|
|**dept**  <br>*optional*|[SysDept](#sysdept)|
|**deptId**  <br>*optional*|integer (int64)|
|**email**  <br>*optional*|string|
|**loginDate**  <br>*optional*|string (date-time)|
|**loginIp**  <br>*optional*|string|
|**nickName**  <br>*optional*|string|
|**params**  <br>*optional*|object|
|**password**  <br>*optional*|string|
|**phonenumber**  <br>*optional*|string|
|**postIds**  <br>*optional*|< integer (int64) > array|
|**remark**  <br>*optional*|string|
|**roleId**  <br>*optional*|integer (int64)|
|**roleIds**  <br>*optional*|< integer (int64) > array|
|**roles**  <br>*optional*|< [SysRoleReq](#sysrolereq) > array|
|**sex**  <br>*optional*|string|
|**status**  <br>*optional*|string|
|**updateBy**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|
|**userId**  <br>*optional*|integer (int64)|
|**userName**  <br>*optional*|string|


<a name="sysuserres"></a>
### SysUserRes

|Name|Schema|
|---|---|
|**admin**  <br>*optional*|boolean|
|**avatar**  <br>*optional*|string|
|**createBy**  <br>*optional*|string|
|**createTime**  <br>*optional*|string (date-time)|
|**delFlag**  <br>*optional*|string|
|**dept**  <br>*optional*|[SysDept](#sysdept)|
|**deptId**  <br>*optional*|integer (int64)|
|**email**  <br>*optional*|string|
|**loginDate**  <br>*optional*|string (date-time)|
|**loginIp**  <br>*optional*|string|
|**nickName**  <br>*optional*|string|
|**params**  <br>*optional*|object|
|**password**  <br>*optional*|string|
|**phonenumber**  <br>*optional*|string|
|**postIds**  <br>*optional*|< integer (int64) > array|
|**remark**  <br>*optional*|string|
|**roleId**  <br>*optional*|integer (int64)|
|**roleIds**  <br>*optional*|< integer (int64) > array|
|**roles**  <br>*optional*|< [SysRoleRes](#sysroleres) > array|
|**sex**  <br>*optional*|string|
|**status**  <br>*optional*|string|
|**updateBy**  <br>*optional*|string|
|**updateTime**  <br>*optional*|string (date-time)|
|**userId**  <br>*optional*|integer (int64)|
|**userName**  <br>*optional*|string|


<a name="sysuserrole"></a>
### SysUserRole

|Name|Schema|
|---|---|
|**roleId**  <br>*optional*|integer (int64)|
|**userId**  <br>*optional*|integer (int64)|


<a name="tabledatainfo"></a>
### TableDataInfo

|Name|Schema|
|---|---|
|**code**  <br>*optional*|integer (int32)|
|**msg**  <br>*optional*|string|
|**rows**  <br>*optional*|< object > array|
|**total**  <br>*optional*|integer (int64)|




<a name="securityscheme"></a>
## Security

<a name="authorization"></a>
### Authorization
*Type* : apiKey  
*Name* : Authorization  
*In* : HEADER



