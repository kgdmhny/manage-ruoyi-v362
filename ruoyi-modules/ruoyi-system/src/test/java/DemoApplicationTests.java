import io.github.swagger2markup.Swagger2MarkupConfig;
import io.github.swagger2markup.Swagger2MarkupConverter;
import io.github.swagger2markup.builder.Swagger2MarkupConfigBuilder;
import io.github.swagger2markup.markup.builder.MarkupLanguage;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.nio.file.Paths;

public class DemoApplicationTests {

    @Test
    public void generateMarkdownDocs() throws Exception {
        //    输出Ascii格式
        Swagger2MarkupConfig config = new Swagger2MarkupConfigBuilder()
                .withMarkupLanguage(MarkupLanguage.MARKDOWN)
                .build();

        Swagger2MarkupConverter.from(new URL("http://localhost:9201/v2/api-docs"))
                .withConfig(config)
                .build()
//                .toFolder(Paths.get("src/test/docs/markdown/generated"));
                .toFile(Paths.get("src/test/docs/markdown/generated/all"));
    }


    @Test
    public void generateAsciiDocs() throws Exception {
        //    输出Ascii格式
        Swagger2MarkupConfig config = new Swagger2MarkupConfigBuilder()
                .withMarkupLanguage(MarkupLanguage.ASCIIDOC)
                .build();

        Swagger2MarkupConverter.from(new URL("http://localhost:9201/v2/api-docs"))
                .withConfig(config)
                .build()
                .toFile(Paths.get("src/test/docs/asciidoc/generated/all"));
    }



}
