import java.util.ArrayList;
import java.util.List;

public class DbDemo {


    public static void main(String[] args) {


        List<String[]> dbInfos = new ArrayList<>();

        //dbname, user, password
        dbInfos.add(new String[]{"multi_admin","xxl_job","aOU!bN42$@Canvi!"});
        dbInfos.add(new String[]{"multi_application_admin_management","admin_management","nYz$3Xj6BXNV&%0@"});
        dbInfos.add(new String[]{"multi_application_access_card","access_card","jg5vR26#GY&GJ1g!"});
        dbInfos.add(new String[]{"multi_application_traffic_card","traffic_card","HWWsKts$5jASAIK^"});
        dbInfos.add(new String[]{"multi_application_campus_card","campus_card","g2cWqWQy1C$1nsQN"});
        dbInfos.add(new String[]{"multi_application_business","simpass_business","ZH$wLZeXyp00DdBx"});
        dbInfos.add(new String[]{"multi_application_kms","multi_kms","cYh9pHsN7km2Crvv"});
        dbInfos.add(new String[]{"multi_application_log_analysis","log_analysis","Kpw0!1FvN^CO9CX1"});
        dbInfos.add(new String[]{"multi_application_nfc","multi_nfc","JAmUFLU^FQRo77Op"});
        dbInfos.add(new String[]{"multi_application_business_script","business_script","Yw$r&M6OrtyH#pJ$"});
        dbInfos.add(new String[]{"multi_application_carkey","multi_carkey","Kra43gMiR7R6tYI5"});


        //# 1、新建数据库
        //create database multi_admin;

//        # 2、新建用户，并授权
//        create user 'xxl_job'@'%' identified with mysql_native_password by 'aOU!bN42$@Canvi!';
//        GRANT ALL PRIVILEGES ON multi_admin.* TO 'xxl_job'@'%';

        System.out.println("# 1、新建数据库");
        for (String[] item :dbInfos)
        {
            System.out.println("create database "+item[0]);
        }


        System.out.println("# 2、新建用户，并授权");

        for (String[] item :dbInfos)
        {
            String userTemplate = "create user '{USER_NAME}'@'%' identified with mysql_native_password by '{PASS_WORD}';";
            String grantTemplate = "GRANT ALL PRIVILEGES ON {DB_NAME}.* TO '{USER_NAME}'@'%';";

            System.out.println(userTemplate.replace("{USER_NAME}",item[1]).replace("{PASS_WORD}",item[2]));
            System.out.println(grantTemplate.replace("{DB_NAME}",item[0]).replace("{USER_NAME}",item[1]));

            System.out.println("");
        }






















































    }


}
