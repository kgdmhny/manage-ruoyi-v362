package entity;

import entity.field.DocFieldInfo;
import lombok.Data;

import java.util.List;

@Data
public class Detail {

    private String opearteName;

    private String httpMethod;
    private String httpUrl;
    private String postDataType;

    //请求参数
    private List<DocFieldInfo> reqParameters;

    //响应参数
    private List<DocFieldInfo> respParameters;


    private String reqSample;

    private String respSample;
}
