package entity;

import lombok.Data;

import java.util.List;

/**
 * 接口信息描述
 * 2.1 数据字典 tags[i].description
 * 2.1.1 数据字典 新增
 *
 * 2.1.2 数据字典 查询
 */
@Data
public class DocInterfaceInfo {

    //接口概要 2级标题
    private String tagDescription;


    //接口描述 3级标题
    private List<Detail> detailList;
}
