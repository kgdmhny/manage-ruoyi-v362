package entity.field;

import lombok.Data;

import java.util.List;

@Data
public class DocFieldInfo {

    private String fieldName;

    private boolean required;

    private String fieldType;

    private String fieldDesc;

    private List<DocFieldInfo> childFieldList;
}
