import entity.Detail;
import entity.DocInterfaceInfo;
import entity.field.DocFieldInfo;
import io.swagger.models.*;
import io.swagger.models.parameters.BodyParameter;
import io.swagger.models.parameters.Parameter;
import io.swagger.models.properties.ArrayProperty;
import io.swagger.models.properties.MapProperty;
import io.swagger.models.properties.Property;
import io.swagger.models.properties.RefProperty;
import io.swagger.parser.SwaggerParser;
import io.swagger.parser.util.SwaggerDeserializationResult;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.collections4.map.LinkedMap;
import world.GenTableWorldServiceImpl;

import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class SwaggerApiDocTest {

    public static final OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.MINUTES)//设置连接超时时间
            .readTimeout(5, TimeUnit.MINUTES)//设置读取超时时间;
            .build();

    public static final MediaType JSONMediaType= MediaType.parse("application/json; charset=utf-8");



    private static final String firstPrefix = ""; //无空格
    private static final String nextPrefix = "  "; //两个空格

    public static void main(String[] args) throws Exception {

        //发送请求获取响应
        Request request = new Request.Builder()
                .url(new URL("http://localhost:9201/v2/api-docs"))
                .build();

        Response resp=okHttpClient.newCall(request).execute();

        String jsonResp = resp.body().string();

        SwaggerParser swaggerParser = new SwaggerParser();
        SwaggerDeserializationResult swaggerDeserializationResult = swaggerParser.readWithInfo(jsonResp);
        Swagger swagger = swaggerDeserializationResult.getSwagger();

        List<DocInterfaceInfo> docInterfaceInfoList = new ArrayList<>();

        Map<String, Path> paths = swagger.getPaths();
        List<Tag> tags = swagger.getTags();

        Map<String, Model> definitions = swagger.getDefinitions();

        for(Tag tag:tags)
        {
            String tagDescription = tag.getDescription();
            String tagName = tag.getName();

            DocInterfaceInfo docInterfaceInfo = new DocInterfaceInfo();
            docInterfaceInfo.setTagDescription(tagDescription);
            List<Detail> details = new ArrayList<>();

            for(String url :paths.keySet())
            {
                /**
                 * 查询属于当前tagName下的所有操作 url
                 */
                Path path = paths.get(url);

                //url 存在的操作
                /**
                 * 例：
                 *  "/config": {
                 *             "post": Object{...},
                 *             "put": Object{...}
                 *         },
                 *  "/config/configKey/{configKey}": {
                 *             "get": Object{...}
                 *         }
                 */
                Operation post = path.getPost();
                Operation delete = path.getDelete();
                Operation put = path.getPut();
                Operation get = path.getGet();

                Map<String,Operation> operationMap = new LinkedMap<>();

                if (post != null) {
                    operationMap.put("POST",post);
                }
                if (delete != null) {
                    operationMap.put("DELETE",delete);
                }
                if (put != null) {
                    operationMap.put("PUT",put);
                }
                if (get != null) {
                    operationMap.put("GET",get);
                }

                for(  String key : operationMap.keySet()) {
                    Operation currentOperation = operationMap.get(key);
                    String httpMethod = key;
                    String currentTag = currentOperation.getTags().get(0);
                    if (tagName.equalsIgnoreCase(currentTag)) {

                        Detail detail = new Detail();
                        detail.setOpearteName(currentOperation.getSummary());
                        detail.setHttpUrl(url);
                        detail.setHttpMethod(httpMethod);
                        if (currentOperation.getConsumes() != null) {
                            detail.setPostDataType(currentOperation.getConsumes().get(0));
                        }
                        System.out.println("url="+url);
                        List<Parameter> parameters = currentOperation.getParameters();
                        if (parameters.size() > 0) {
                            Parameter parameter = parameters.get(0);
                            String in = parameter.getIn();
                            if(in.equals("body"))
                            {
                                BodyParameter bodyParameter = (BodyParameter) parameter;
                                Model schema = bodyParameter.getSchema();
                                if(schema instanceof ModelImpl)
                                {

                                    String format = ((ModelImpl)schema).getFormat();
                                    String type =((ModelImpl)schema).getType();
                                    if(type.equals("string")&&format.equals("binary")){
                                        // 当上传文件时，常用的方式是通过 HTTP 请求将文件内容作为二进制数据发送到服务器。
                                        // 在 Swagger（OpenAPI）规范中，针对这种情况，通常会使用 string 类型来表示文件上传字段，并将其格式（format）设置为 binary
                                        DocFieldInfo docFieldInfo = new DocFieldInfo();
                                        docFieldInfo.setFieldName(bodyParameter.getName());
                                        docFieldInfo.setRequired(bodyParameter.getRequired());
                                        docFieldInfo.setFieldType("file");
                                        docFieldInfo.setFieldDesc(bodyParameter.getDescription());
                                        List<DocFieldInfo> docFieldInfoList = new ArrayList<>();
                                        docFieldInfoList.add(docFieldInfo);
                                    }
                                }
                                Set<String> refObjectSet = new HashSet<>();
                                List<DocFieldInfo> docFieldInfoList =  getDocFieldInfoListByModelImpl(firstPrefix,schema,definitions,refObjectSet);
                                //将params参数移至list 最后一个,好看点
                                for(int k=0;k<docFieldInfoList.size();k++){
                                    DocFieldInfo docFieldInfo = docFieldInfoList.get(k);
                                    String fieldName = docFieldInfo.getFieldName();
                                    String fieldType = docFieldInfo.getFieldType();
                                    if(fieldName.equals("params")&&fieldType.startsWith("ref["))
                                    {
                                        //  移除 i 位置
                                        docFieldInfoList.remove(k);
                                        //放到最后一个位置
                                        docFieldInfoList.add(docFieldInfo);
                                    }
                                }

                                detail.setReqParameters(docFieldInfoList);
                            }
                        }

                        io.swagger.models.Response response = currentOperation.getResponses().get("200");


                        Property schema = response.getSchema();

                        if(schema instanceof MapProperty)
                        {
                            //返回的是map类型
                            List<DocFieldInfo> respParameters = new ArrayList<>();
                            DocFieldInfo docFieldInfo = new DocFieldInfo();
                            docFieldInfo.setFieldName("code");
                            docFieldInfo.setFieldDesc("状态码");
                            docFieldInfo.setFieldType("int");
                            docFieldInfo.setRequired(true);

                            DocFieldInfo docFieldInfo2 = new DocFieldInfo();
                            docFieldInfo2.setFieldName("msg");
                            docFieldInfo2.setFieldDesc("状态码描述");
                            docFieldInfo2.setFieldType("string");
                            docFieldInfo2.setRequired(true);

                            DocFieldInfo docFieldInfo3 = new DocFieldInfo();
                            docFieldInfo3.setFieldName("data");
                            docFieldInfo3.setFieldDesc("响应信息");
                            docFieldInfo3.setFieldType("object");
                            docFieldInfo3.setRequired(false);
                            respParameters.add(docFieldInfo);
                            respParameters.add(docFieldInfo2);
                            respParameters.add(docFieldInfo3);
                            detail.setRespParameters(respParameters);

                        }else {

                            Model responseSchema = response.getResponseSchema();

                            if (responseSchema != null) {
                                Set<String> refObjectSet = new HashSet<>();
                                List<DocFieldInfo> respDocFieldInfoList = getDocFieldInfoListByModelImpl(firstPrefix, responseSchema, definitions, refObjectSet);
                                detail.setRespParameters(respDocFieldInfoList);
                            }
                        }


                        details.add(detail) ;
                    }
                }

            };

            docInterfaceInfo.setDetailList(details);

            docInterfaceInfoList.add(docInterfaceInfo);

        }


        GenTableWorldServiceImpl.generateWorld(docInterfaceInfoList);

    }

    private static List<DocFieldInfo> getDocFieldInfoListByModelImpl(String prefix, Model model, Map<String, Model> definitions , Set<String> refObjectSet) {

        String simpleRef = "";
        ModelImpl modelImpl ;
        if(model instanceof RefModel) {
            RefModel refModel = (RefModel) model;
            simpleRef = refModel.getSimpleRef();
            modelImpl= (ModelImpl) definitions.get(simpleRef);
            refObjectSet.add(simpleRef);

        }else if(model instanceof  ModelImpl){
            modelImpl =  (ModelImpl) model;
        }else {
            throw new RuntimeException("model 实现类暂未定义！");
        }


        List<DocFieldInfo> docFieldInfoList = new ArrayList<>();
        Map<String, Property> properties = modelImpl.getProperties();
        if(properties != null) {
            for (String key : properties.keySet()) {
                Property property = properties.get(key);

                DocFieldInfo docFieldInfo = new DocFieldInfo();
                docFieldInfo.setFieldName(prefix + key);
                docFieldInfo.setRequired(property.getRequired());
                String fieldType = property.getType();
                docFieldInfo.setFieldType(fieldType);
                docFieldInfo.setFieldDesc(property.getDescription());
                if (fieldType.equalsIgnoreCase("object")) {


//                docFieldInfo.setChildFieldList(getDocFieldInfoListByModelImpl(prefix+nextPrefix,modelImpl,definitions,refObjectSet));
                    if (!refObjectSet.contains(simpleRef)) {
                        //防止循环依赖的递归调用陷入死循环
                        refObjectSet.add(simpleRef);
                        docFieldInfo.setChildFieldList(getDocFieldInfoListByModelImpl(prefix + nextPrefix, modelImpl, definitions, refObjectSet));

                    } else {
                        //标记循环依赖某个对象
                        docFieldInfo.setFieldType("ref[" + simpleRef + "]");
                    }

                } else if (fieldType.equalsIgnoreCase("array")) {
                    ArrayProperty arrayProperty = (ArrayProperty) property;
                    Property items = arrayProperty.getItems();
                    if (items instanceof RefProperty) {
                        RefProperty refProperty = (RefProperty) arrayProperty.getItems();
                        simpleRef = refProperty.getSimpleRef();
                        docFieldInfo.setFieldType("List<" + simpleRef + ">");
                        modelImpl = (ModelImpl) definitions.get(simpleRef);
                        if (!refObjectSet.contains(simpleRef)) {
                            docFieldInfo.setChildFieldList(getDocFieldInfoListByModelImpl(prefix + nextPrefix, modelImpl, definitions, refObjectSet));
                        }
                    }

                } else if (fieldType.equalsIgnoreCase("ref")) {
                    RefProperty refProperty = (RefProperty) property;
                    simpleRef = refProperty.getSimpleRef();
                    modelImpl = (ModelImpl) definitions.get(simpleRef);
                    docFieldInfo.setFieldType("Object<" + simpleRef + ">");

                    if (!refObjectSet.contains(simpleRef)) {
                        //防止循环依赖的递归调用陷入死循环
                        refObjectSet.add(simpleRef);
                        docFieldInfo.setChildFieldList(getDocFieldInfoListByModelImpl(prefix + nextPrefix, modelImpl, definitions, refObjectSet));

                    } else {
                        //标记循环依赖某个对象
                        docFieldInfo.setFieldType("ref[" + simpleRef + "]");
                    }
                }

                docFieldInfoList.add(docFieldInfo);
            }
        }


        return docFieldInfoList;

    }
}
