package world;


import entity.field.DocFieldInfo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;


public class TableWorldUtil {

    public static void createTable(int rowNum, DocFieldInfo docFieldInfo, XWPFTable table) throws Exception {


        if (rowNum == 0) {

            XWPFTableRow titleRow = table.getRow(1);

            setWorldCell(table,docFieldInfo, titleRow);

        } else {

            XWPFTableRow titleRow2 = table.createRow();

            setWorldCell(table,docFieldInfo, titleRow2);



        }


    }






    private static void setWorldCell(XWPFTable table,DocFieldInfo docFieldInfo, XWPFTableRow titleRow2) {
        titleRow2.getTableCells().get(0).setText(docFieldInfo.getFieldName().toLowerCase()); //字段名称

        titleRow2.getTableCells().get(1).setText(docFieldInfo.isRequired()+"");//是否必选
        titleRow2.getTableCells().get(2).setText(docFieldInfo.getFieldType());//字段类型
        titleRow2.getTableCells().get(3).setText(docFieldInfo.getFieldType());//说明

        if(CollectionUtils.isNotEmpty(docFieldInfo.getChildFieldList()))
        {

            for(DocFieldInfo childDocFieldInfo:docFieldInfo.getChildFieldList())
            {
                XWPFTableRow titleRow_child = table.createRow();

                setWorldCell(table,childDocFieldInfo, titleRow_child);
            }
        }

    }

    public static void setCellValue(XWPFTable table, int rowIndex, int colIndex, String[] comments) {
        XWPFTableCell cell = table.getRow(rowIndex).getCell(colIndex);
        XWPFParagraph paragraph = cell.getParagraphs().get(0);
        XWPFRun cellRun = paragraph.createRun();
        for (int k=0;k< comments.length;k++) {
            cellRun.setText(comments[k]);
            if(k!=comments.length-1) {
                cellRun.addBreak();
            }
        }
    }

    public static void setCellValue(XWPFTable table, int rowIndex, int colIndex, String value) {
        setCellValue(table,rowIndex,colIndex,new String[]{value});
    }

}
