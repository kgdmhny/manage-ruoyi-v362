package world;


import entity.Detail;
import entity.DocInterfaceInfo;
import entity.field.DocFieldInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.List;


@Service(value = "genTableWorldService")
@Slf4j
public class GenTableWorldServiceImpl {


    public GenTableWorldServiceImpl() throws FileNotFoundException {
    }

    public static void generateWorld(List<DocInterfaceInfo> docInterfaceInfoList) throws Exception {


        ClassLoader classLoader = GenTableWorldServiceImpl.class.getClassLoader();
        XWPFDocument document_model = new XWPFDocument(classLoader.getResourceAsStream("world/接口文档模板.docx"));// 生成word文档并读取模板


        CTTbl ctTbl_model_1 = document_model.getTables().get(0).getCTTbl();
        IBody iBody_model_1 = document_model.getTables().get(0).getBody();

        CTTbl ctTbl_model_2 = document_model.getTables().get(1).getCTTbl();
        IBody iBody_model_2 = document_model.getTables().get(1).getBody();

        CTTbl ctTbl_model_3 = document_model.getTables().get(2).getCTTbl();
        IBody iBody_model_3 = document_model.getTables().get(2).getBody();

        CTTbl ctTbl_model_4 = document_model.getTables().get(3).getCTTbl();
        IBody iBody_model_4 = document_model.getTables().get(3).getBody();

        CTTbl ctTbl_model_5 = document_model.getTables().get(4).getCTTbl();
        IBody iBody_model_5 = document_model.getTables().get(4).getBody();


        XWPFDocument document;
        InputStream resourceAsStream = GenTableWorldServiceImpl.class.getClassLoader().getResourceAsStream("world/table1.docx");
        document = new XWPFDocument(resourceAsStream);// 生成word文档并读取模板

        int tableNum = 0;

        for (int i = 0; i < docInterfaceInfoList.size(); i++) {

//            if (i != 0) {
//                //留一行空
//                document.createParagraph();
//            }
            //接口Controller描述信息
            DocInterfaceInfo interfaceInfo = docInterfaceInfoList.get(i);
            String tagDescription = interfaceInfo.getTagDescription();

            XWPFParagraph paragraphTag = document.createParagraph();
            paragraphTag.setStyle("3");
            XWPFRun RunTag = paragraphTag.createRun();
            RunTag.setText(tagDescription);

            List<Detail> detailList = interfaceInfo.getDetailList();
            if (CollectionUtils.isNotEmpty(detailList)) {
                for (int j = 0; j < detailList.size(); j++) {
                    Detail detail = detailList.get(j);
                    XWPFParagraph paragraph = document.createParagraph();

                    paragraph.setStyle("4");
                    XWPFRun Run = paragraph.createRun();
                    Run.setText(detail.getOpearteName());

                    XWPFParagraph paragraph_3_1 = document.createParagraph();
                    XWPFRun Run_3_1 = paragraph_3_1.createRun();
                    Run_3_1.setText("\uF06C接口调用请求说明");
                    CTTbl ctTbl_3_1 = CTTbl.Factory.newInstance(); // 创建新的 CTTbl ， table
                    ctTbl_3_1.set(ctTbl_model_1); // 复制原来的CTTbl
                    XWPFTable newTable_3_1 = new XWPFTable(ctTbl_3_1, iBody_model_1); // 新增一个table，使用复制好的Cttbl
                    document.createTable(); // 创建一个空的Table
                    String postContentTemplate = "http请求方式: {httpMethod};" +
                            "http调用地址：{httpUrl};" +
                            "POST数据格式：{postDataType}";
                    String postContent = postContentTemplate.replace("{httpMethod}", detail.getHttpMethod())
                            .replace("{httpUrl}", detail.getHttpUrl())
                            .replace("{postDataType}", detail.getPostDataType() != null ? detail.getPostDataType() : "");
                    TableWorldUtil.setCellValue(newTable_3_1, 0, 0, postContent.split(";"));
                    document.setTable(tableNum++, newTable_3_1); // 将table设置到word中

                    XWPFParagraph paragraph_3_2 = document.createParagraph();
                    XWPFRun Run_3_2 = paragraph_3_2.createRun();
                    Run_3_2.setText("\uF06C请求示例");
                    CTTbl ctTbl_3_2 = CTTbl.Factory.newInstance(); // 创建新的 CTTbl ， table
                    ctTbl_3_2.set(ctTbl_model_2); // 复制原来的CTTbl
                    XWPFTable newTable_3_2 = new XWPFTable(ctTbl_3_2, iBody_model_2); // 新增一个table，使用复制好的Cttbl
                    document.createTable();// 创建一个空的Table
                    TableWorldUtil.setCellValue(newTable_3_2, 0, 0, "");
                    document.setTable(tableNum++, newTable_3_2); // 将table设置到word中

                    List<DocFieldInfo> docFieldInfoList = detail.getReqParameters();
                    XWPFTable newTable_3_3 = null;
                    if (CollectionUtils.isNotEmpty(docFieldInfoList)) {
                        for (int k = 0; k < docFieldInfoList.size(); k++) {
                            if (k == 0) {
                                XWPFParagraph paragraph_3_3 = document.createParagraph();
                                XWPFRun Run_3_3 = paragraph_3_3.createRun();
                                Run_3_3.setText("\uF06C请求参数说明");
                                CTTbl ctTbl_3_3 = CTTbl.Factory.newInstance(); // 创建新的 CTTbl ， table
                                ctTbl_3_3.set(ctTbl_model_3); // 复制原来的CTTbl
                                newTable_3_3 = new XWPFTable(ctTbl_3_3, iBody_model_3); // 新增一个table，使用复制好的Cttbl
                                document.createTable(); // 创建一个空的Table
                            }
                            TableWorldUtil.createTable(k, docFieldInfoList.get(k), newTable_3_3);
                        }
                        document.setTable(tableNum++, newTable_3_3); // 将table设置到word中
                    }

                    XWPFParagraph paragraph_3_4 = document.createParagraph();
                    XWPFRun Run_3_4 = paragraph_3_4.createRun();
                    Run_3_4.setText("\uF06C返回示例");
                    CTTbl ctTbl_3_4 = CTTbl.Factory.newInstance(); // 创建新的 CTTbl ， table
                    ctTbl_3_4.set(ctTbl_model_4); // 复制原来的CTTbl
                    XWPFTable newTable_3_4 = new XWPFTable(ctTbl_3_4, iBody_model_4); // 新增一个table，使用复制好的Cttbl
                    document.createTable(); // 创建一个空的Table
                    TableWorldUtil.setCellValue(newTable_3_4, 0, 0, "");
                    document.setTable(tableNum++, newTable_3_4); // 将table设置到word中

                    List<DocFieldInfo> respParametersList = detail.getRespParameters();
                    XWPFTable newTable_3_5 = null;
                    if (CollectionUtils.isNotEmpty(respParametersList)) {
                        for (int k = 0; k < respParametersList.size(); k++) {
                            if (k == 0) {
                                XWPFParagraph paragraph_3_5 = document.createParagraph();
                                XWPFRun Run_3_5 = paragraph_3_5.createRun();
                                Run_3_5.setText("\uF06C返回内容字段");
                                CTTbl ctTbl_3_5 = CTTbl.Factory.newInstance(); // 创建新的 CTTbl ， table
                                ctTbl_3_5.set(ctTbl_model_3); // 复制原来的CTTbl
                                newTable_3_5 = new XWPFTable(ctTbl_3_5, iBody_model_5); // 新增一个table，使用复制好的Cttbl
                                document.createTable(); // 创建一个空的Table
                            }
                            TableWorldUtil.createTable(k, respParametersList.get(k), newTable_3_5);
                        }
                        document.setTable(tableNum++, newTable_3_5); // 将table设置到word中
                    }



                }
            }

        }


        ByteArrayOutputStream os = new ByteArrayOutputStream();
//        String AbsolutePath = new File("").getAbsolutePath()+"/admin-management/admin-modules/admin-system/";
        String AbsolutePath = new File("").getAbsolutePath()+"/ruoyi-modules/ruoyi-system/";
        FileOutputStream f = new FileOutputStream(AbsolutePath+ "src\\test\\resources\\world\\table2.docx");

        document.write(os);
        f.write(os.toByteArray());
        f.close();
        os.close();


        System.out.println("success");


        }


    }
