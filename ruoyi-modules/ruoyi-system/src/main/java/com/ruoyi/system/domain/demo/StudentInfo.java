package com.ruoyi.system.domain.demo;

import lombok.Data;

import java.util.List;

@Data
public class StudentInfo {

    private int age;
    private String name;

    //学生的选课信息
    private List<Course> courses;
}
