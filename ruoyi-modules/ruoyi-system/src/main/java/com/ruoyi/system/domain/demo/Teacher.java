package com.ruoyi.system.domain.demo;

import lombok.Data;

@Data
public class Teacher {

    private int teacherId;

    private String name;

    private String educationBackground;

    //老师最喜欢的一个学生
    private StudentInfo favoriteStudent;




}
