package com.ruoyi.system.controller.demo;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.SysConfig;
import com.ruoyi.system.domain.demo.StudentInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Api(description = "Demo测试配置")
@RestController
@RequestMapping("/demo")
public class DemoController  extends BaseController {

    /**
     * 修改参数配置
     */
    @ApiOperation("修改学生信息")
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody StudentInfo studentInfo)
    {
        StudentInfo studentInfo1 = new StudentInfo();
        studentInfo1.setAge(111);
        studentInfo1.setName("张三");
        return success(studentInfo1);
    }

    @ApiOperation("获取学生信息")
    @GetMapping("/list")
    public AjaxResult edit2(@Validated @RequestBody StudentInfo studentInfo)
    {
        StudentInfo studentInfo1 = new StudentInfo();
        studentInfo1.setAge(111);
        studentInfo1.setName("张三");
        return success(studentInfo1);
    }
}
