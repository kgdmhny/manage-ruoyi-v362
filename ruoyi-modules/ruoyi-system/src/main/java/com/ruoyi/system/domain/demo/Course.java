package com.ruoyi.system.domain.demo;

import lombok.Data;

@Data
public class Course {

    private int courseId;

    //课程对应的讲课老师
    private Teacher teacher;
}
